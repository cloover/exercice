# devops-test

## BRANCHES

- LA BRANCHE "NEXT" SERT A TESTER DES FUTURES FONCTIONALITES AVANT DE LES DEPLOYER, 
  ELLE NE PERMET QUE DES "MERGES REQUESTS" ET PAS DE PUSH DIRECTEMENT.
- LA BRANCHE "PRODUCTION" SERT A DEPLOYER UNE OU PLUSIEURES FONCTINALITÉ VALIDÉS.

## PROCESSUS DU DEPLOIEMENT

| DEV | --> PUSH --> | NEXT-FEATURE | --> MERGE REQUEST --> | NEXT | --> CI TESTS --> MERGE --> CI STAGING --> MERGE REQUEST --> | PRODUCTION | --> MERGE --> | DEPLOY |

## ARCHITECTURE DU DEPLOIEMENT

LE DEPLOIEMENT SE FAIT PAR LA CONSTRUCTION DES CONTENAIRES PAR GITLAB-CI QUI SONT ENSUITE PUSHER VERS LE REGISTER OPENSHIFT,
QUI SE SERT DE CES IMAGES POUR LANCER L'APPLICATION DANS 2 DEFFIRENTS ENVIRONNEMENTS.

__*OPENSHIFT EST STRUCTURé COMME SUIT :*__

        | PRODUCTION ENDPOINT |                                     | STAGING ENDPOINT |
        -----------------------                                     --------------------
                 |                                                           |
             | PODS |                                                    | PODS |
                 |                                                           |
    |INTERNAL PRODUCTION REDIS DB SERVICE|                  |INTERNAL STAGING REDIS DB SERVICE|

## TODO
- VERIFIER LA STRATEGIE DE CREATION DE PODS DANS OPENSHIFT AVEC L'ENVIRONNEMENT PRODUCTION.

## BUGS
- IL FAUT CREER AUTOMATIQUEMENT UNE NOUVELLE MERGE REQUEST 
   A LA FIN DE LA PHASE STAGING DANS GITLAB-CI 
   ( *Cette merge request sert a validé le travail fait dans l'environnement STAGING et lancer le dployement dans l'environnement production* ). #gitlab-ci/.gitlab-ci.yml  L:45