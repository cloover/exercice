package fr.adservio.clickcount;

import javax.inject.Singleton;

@Singleton
public class Configuration {

    public final String redisHost;
    public final String redisPassword;
    public final int redisPort;
    public final int redisConnectionTimeout;  //milliseconds

    public Configuration() {        
        redisHost = System.getenv("REDIS_HOST");    
        redisPort = 6379;
        redisPassword = System.getenv("REDIS_PASS");
        redisConnectionTimeout = 2000;
    }
}
