#!/usr/bin/env bash

HOST="https://gitlab.com/api/v4/projects/"

BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"next\",
    \"target_branch\": \"production\",
    \"remove_source_branch\": false,
    \"title\": \"MISE EN PRODUCTION WIP: ${CI_COMMIT_MESSAGE}\",
    \"assignee_id\":\"${GITLAB_USER_ID}\"
}";

res=$(curl -s -v -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
    --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}" | jq -r '.id') && if [[ $res = null ]]; then exit 1; else echo "success"; fi;